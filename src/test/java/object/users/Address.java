package object.users;

public class Address {
    public String address;
    public String city;
    public Coordinates coordinates;
    public String postalCode;
    public String state;
}
