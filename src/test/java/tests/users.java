package tests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/features/users.feature",
        tags = {"@finalizado"},
        glue = {""},
        monochrome = true,
        dryRun = false)

public class users {

}
