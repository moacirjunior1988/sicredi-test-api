  # language: pt
  Funcionalidade: Login

    @Login @finalizado
    Cenario: [API login | @positivo] - Validar retorno da api post login
      Dado que monto o payload de post da api de login
        | header   | payload | username   | password |
        | semToken | correto | kminchelle | 0lelplR  |
      Quando executo a requisicao com alguma das acoes na api
        | pathApi    | operation | parametroGet |
        | auth/login | post      |              |
      Entao valido o retorno da api login
        | statusCodeEsperado |
        |                200 |