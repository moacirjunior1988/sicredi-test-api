  # language: pt
  Funcionalidade: Auth Products

    Contexto: Pré requisito para execução
      Dado que monto o payload de post da api de login
        | header   | payload | username   | password |
        | semToken | correto | kminchelle | 0lelplR  |
      Quando executo a requisicao com alguma das acoes na api
        | pathApi    | operation | parametroGet |
        | auth/login | post      |              |
      Entao valido o retorno da api login
        | statusCodeEsperado |
        |                200 |

    @AuthProducts @finalizado
    Cenario: [API auth products | @positivo] - Validar retorno da api get auth products
      Dado que monto o payload de get da api de auth products
        | header   |
        | comToken |
      Quando executo a requisicao com alguma das acoes na api
        | pathApi        | operation | parametroGet |
        | auth/products/ | get       |              |
      Entao valido o retorno da api
        | statusCodeEsperado |
        |                200 |

    @AuthProducts @finalizado
    Cenario: [API users | @negativo] - Validar retorno da api get auth products com parametro inválido
      Dado que monto o payload de get da api de auth products
        | header   |
        | semToken |
      Quando executo a requisicao com alguma das acoes na api
        | pathApi        | operation       | parametroGet |
        | auth/products/ | getcomparametro | oi           |
      Entao valido o retorno da api
        | statusCodeEsperado |
        | 403                |

    @AuthProducts @finalizado
    Cenario: [API auth products | @negativo] - Validar retorno da api get auth products sem autenticação
      Dado que monto o payload de get da api de auth products
        | header   |
        | semToken |
      Quando executo a requisicao com alguma das acoes na api
        | pathApi        | operation | parametroGet |
        | auth/products/ | get       |              |
      Entao valido o retorno da api
        | statusCodeEsperado |
        | 403                |

    @AuthProducts @finalizado
    Cenario: [API auth products | @negativo] - Validar retorno da api get auth products com autenticação inválida
      Dado que monto o payload de get da api de auth products
        | header           |
        | comTokenInvalido |
      Quando executo a requisicao com alguma das acoes na api
        | pathApi        | operation | parametroGet |
        | auth/products/ | get       |              |
      Entao valido o retorno da api
        | statusCodeEsperado |
        | 401                |