  # language: pt
  Funcionalidade: Products

    @Teste @finalizado
    Cenario: [API product | @positivo] - Validar cadastrar novo produto na api post product add
      Dado que monto o payload de post da api de product
        | header   | payload | title  | description         | price   | discountPercentage | rating  | stock   | brand  | category    | thumbnail                                             |
        | semToken | correto | Realme | Um exelente celular | inserir | inserir            | inserir | inserir | Realme | smartphones | https://i.dummyjson.com/data/products/12/thumnail.jpg |
      Quando executo a requisicao com alguma das acoes na api
        | pathApi      | operation | parametroGet |
        | products/add | post      |              |
      Entao valido o retorno da api
        | statusCodeEsperado |
        |                200 |

    @Teste @finalizado
    Cenario: [API product | @positivo] - Validar cadastrar novo produto na api post product add sem passar parâmetros
      Dado que monto o payload de post da api de product
        | header   | payload    |  |  |  |  |  |  |  |  |  |
        | semToken | semPayload |  |  |  |  |  |  |  |  |  |
      Quando executo a requisicao com alguma das acoes na api
        | pathApi      | operation | parametroGet |
        | products/add | post      |              |
      Entao valido o retorno da api
        | statusCodeEsperado |
        | 400                |

    @Teste @finalizado
    Cenario: [API product | @positivo] - Validar retorno da api get product
      Dado que monto o payload de get da api de product
        | header   |
        | semToken |
      Quando executo a requisicao com alguma das acoes na api
        | pathApi   | operation | parametroGet |
        | products/ | get       |              |
      Entao valido o retorno da api
        | statusCodeEsperado |
        |                200 |

    @Teste @finalizado
    Cenario: [API product | @positivo] - Validar retorno da api get product com passagem de parâmetro
      Dado que monto o payload de get da api de product
        | header   |
        | semToken |
      Quando executo a requisicao com alguma das acoes na api
        | pathApi   | operation       | parametroGet |
        | products/ | getcomparametro | 29           |
      Entao valido o retorno da api
        | statusCodeEsperado |
        |                200 |

    @Teste @finalizado
    Cenario: [API product | @negativo] - Validar retorno da api get product com passagem de parâmetro inválido
      Dado que monto o payload de get da api de product
        | header   |
        | semToken |
      Quando executo a requisicao com alguma das acoes na api
        | pathApi   | operation       | parametroGet |
        | products/ | getcomparametro | teste        |
      Entao valido o retorno da api
        | statusCodeEsperado |
        | 404                |