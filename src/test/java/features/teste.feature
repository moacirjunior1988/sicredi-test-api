  # language: pt
  Funcionalidade: Teste

    @Teste @finalizado
    Cenario: [API teste | @positivo] - Validar retorno da api get teste
      Dado que monto o payload de get da api de teste
        | header   |
        | semToken |
      Quando executo a requisicao com alguma das acoes na api
        | pathApi | operation | parametroGet |
        | test/   | get       |              |
      Entao valido o retorno da api
        | statusCodeEsperado |
        | 200                |