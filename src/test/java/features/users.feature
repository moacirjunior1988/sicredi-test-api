  # language: pt
  Funcionalidade: Users

    @Users @finalizado
    Cenario: [API users | @positivo] - Validar retorno da api get users
      Dado que monto o payload de get da api de users
        | header   |
        | semToken |
      Quando executo a requisicao com alguma das acoes na api
        | pathApi | operation | parametroGet |
        | users/  | get       |              |
      Entao valido o retorno da api
        | statusCodeEsperado |
        |                200 |

    @Users @finalizado
    Cenario: [API users | @negativo] - Validar retorno da api get users com parametro inválido
      Dado que monto o payload de get da api de users
        | header   |
        | semToken |
      Quando executo a requisicao com alguma das acoes na api
        | pathApi | operation       | parametroGet |
        | users/  | getcomparametro | oi           |
      Entao valido o retorno da api
        | statusCodeEsperado |
        |                400 |