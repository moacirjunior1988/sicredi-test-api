package steps;

import com.aventstack.extentreports.ExtentTest;
import com.google.gson.Gson;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import geral.hooks;
import geral.utils;
import io.restassured.RestAssured;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class testeSteps
{
    protected int statusCodeApi;
    protected ExtentTest extentTest;
    private final stepBase stepBase;
    geral.utils utils = new utils();

    public testeSteps(stepBase stepBase)
    {
        this.stepBase = stepBase;
        this.extentTest = hooks.test;
        stepBase.gson = new Gson();
    }

    @Given("que monto o payload de get da api de teste")
    public void dadoQueMontoOPayloadDeGetDaApiDeTeste(DataTable dataTable)
    {
        extentTest.info("Dado que monto o payload de get da api de teste.");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);

        switch (data.get(0).get("header"))
        {
            case "comToken":
                stepBase.request = RestAssured.given()
                        .header("Authorization", "Bearer" + stepBase.token)
                        .contentType("application/json")
                        .body(stepBase.json);
                break;

            case "semToken":
                stepBase.request = RestAssured.given()
                        .contentType("application/json");
                break;
        }
    }

    @When("executo a requisicao com alguma das acoes na api$")
    public void quandoExecutoARequisicaoComAlgumaDasAcoesNaApi(DataTable dataTable)
    {
        extentTest.info("Quando executo a requisicao com alguma das ações na api.");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);

        try
        {
            System.out.println("Final url: " + stepBase.baseUri + data.get(0).get("pathApi"));
            switch (data.get(0).get("operation").toLowerCase())
            {
                case "get":
                    System.out.println("Final url: " + stepBase.baseUri + data.get(0).get("pathApi"));
                    stepBase.response = stepBase.request.when().get(stepBase.baseUri + data.get(0).get("pathApi"));
                    break;

                case "getcomparametro":
                    System.out.println("Final url: " + stepBase.baseUri + data.get(0).get("pathApi") + data.get(0).get("parametroGet"));
                    stepBase.response = stepBase.request.when().get(stepBase.baseUri + data.get(0).get("pathApi") + data.get(0).get("parametroGet"));
                    break;

                case "post":
                    stepBase.response = stepBase.request.when().post(stepBase.baseUri + data.get(0).get("pathApi"));
                    break;

                case "put":
                    stepBase.response = stepBase.request.when().put(stepBase.baseUri + data.get(0).get("pathApi"));
                    break;

                case "delete":
                    stepBase.response = stepBase.request.when().delete(stepBase.baseUri + data.get(0).get("pathApi") + stepBase.id);
                    break;
            }
            System.out.println("Body de retorno da api: " + stepBase.response.then().extract().asString());
        }
        catch (Exception e)
        {
            extentTest.fail("Quando executo a requisicao com alguma das ações na api.");
            extentTest.warning("Erro: " + e.getMessage());
        }
    }

    @Then("valido o retorno da api$")
    public void EntaoValidoORetornoDaApi(DataTable dataTable)
    {
        extentTest.info("Então valido o retorno da api");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        int statuCode = Integer.parseInt(data.get(0).get("statusCodeEsperado"));
        statusCodeApi = stepBase.response.getStatusCode();

        System.out.println("Status: " + statusCodeApi);
        switch (statusCodeApi)
        {
            case 200:
                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                break;

            case 201:
                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                break;

            case 202:
                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                break;

            case 204:
                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                break;

            case 400:
                stepBase.msg = stepBase.response.then().extract().response().path("error") + ": " +
                        stepBase.response.then().extract().response().path("message");

                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                System.out.println("Return msn: " + stepBase.msg);
                break;

            case 401:
                stepBase.msg = stepBase.response.then().extract().response().path("error") + ": " +
                        stepBase.response.then().extract().response().path("message");

                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                System.out.println("Return msn: " + stepBase.msg);
                break;

            case 403:
                stepBase.msg = stepBase.response.then().extract().response().path("error") + ": " +
                        stepBase.response.then().extract().response().path("message");

                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                System.out.println("Return msn: " + stepBase.msg);
                break;

            case 404:
                stepBase.msg = stepBase.response.then().extract().response().path("error") + ": " +
                        stepBase.response.then().extract().response().path("message");

                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                System.out.println("Return msn: " + stepBase.msg);
                break;

            default:
                System.out.println("Status Code: " + statusCodeApi + " retornado pela api não previsto para tratamento!");
                extentTest.fail("Status Code: \" + statusCodeApi + \" retornado pela api não previsto para tratamento!");
                Assert.assertTrue(false);
                break;
        }
    }
}
