package steps;

import com.aventstack.extentreports.ExtentTest;
import com.google.gson.Gson;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import geral.hooks;
import geral.utils;
import io.restassured.RestAssured;
import object.loginPost;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class loginSteps {

    protected int statusCodeApi;
    protected ExtentTest extentTest;
    private final stepBase stepBase;

    private loginPost loginPost;
    geral.utils utils = new utils();

    public loginSteps(stepBase stepBase)
    {
        this.stepBase = stepBase;
        this.extentTest = hooks.test;
        this.loginPost = new loginPost();
        stepBase.gson = new Gson();
    }

    @Given("^que monto o payload de post da api de login$")
    public void queMontoOPayloadDePostDaApiDeLogin(DataTable dataTable) {
        extentTest.info("Dado que monto o payload de get da api de teste.");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);

        switch (data.get(0).get("payload"))
        {
            case "correto":
            {
                loginPost.username = data.get(0).get("username");
                loginPost.password = data.get(0).get("password");
            }
            break;

            case "semUserName": {
                loginPost.password = data.get(0).get("password");
            }
            break;

            case "semPasword": {
                loginPost.username = data.get(0).get("username");
            }
            break;

            case "semPayload":
                loginPost = null;
                break;
        }

        stepBase.json = stepBase.gson.toJson(loginPost);

        System.out.println("Body montado: " + stepBase.json);


        switch (data.get(0).get("header"))
        {
            case "semToken":
                stepBase.request = RestAssured.given()
                        .contentType("application/json")
                        .body(stepBase.json);
                break;
        }
    }

    @Then("valido o retorno da api login$")
    public void EntaoValidoORetornoDaApiLogin(DataTable dataTable)
    {
        extentTest.info("Então valido o retorno da api");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        int statuCode = Integer.parseInt(data.get(0).get("statusCodeEsperado"));
        statusCodeApi = stepBase.response.getStatusCode();

        switch (statusCodeApi)
        {
            case 200:
                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                stepBase.token = stepBase.response.then().extract().path("token");
                break;

            case 201:
                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                break;

            case 202:
                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                break;

            case 204:
                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                break;

            case 400:
                stepBase.msg = stepBase.response.then().extract().response().path("error") + ": " +
                        stepBase.response.then().extract().response().path("message");

                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                System.out.println("Return msn: " + stepBase.msg);
                break;

            case 404:
                stepBase.msg = stepBase.response.then().extract().response().path("error") + ": " +
                        stepBase.response.then().extract().response().path("message");

                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                System.out.println("Return msn: " + stepBase.msg);
                break;

            default:
                System.out.println("Status Code: " + statusCodeApi + " retornado pela api não previsto para tratamento!");
                extentTest.fail("Status Code: \" + statusCodeApi + \" retornado pela api não previsto para tratamento!");
                Assert.assertTrue(false);
                break;
        }
    }
}
