package steps;

import com.aventstack.extentreports.ExtentTest;
import com.google.gson.Gson;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import geral.hooks;
import geral.utils;
import io.restassured.RestAssured;

import java.util.List;
import java.util.Map;

public class usersSteps
{
    protected int statusCodeApi;
    protected ExtentTest extentTest;
    private final stepBase stepBase;
    geral.utils utils = new utils();

    public usersSteps(stepBase stepBase)
    {
        this.stepBase = stepBase;
        this.extentTest = hooks.test;
        stepBase.gson = new Gson();
    }

    @Given("que monto o payload de get da api de users")
    public void dadoQueMontoOPayloadDeGetDaApiDeUsers(DataTable dataTable)
    {
        extentTest.info("Dado que monto o payload de get da api de users.");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);

        switch (data.get(0).get("header"))
        {
            case "comToken":
                stepBase.request = RestAssured.given()
                        .header("Authorization", "Bearer" + stepBase.token)
                        .contentType("application/json")
                        .body(stepBase.json);
                break;

            case "semToken":
                stepBase.request = RestAssured.given()
                        .contentType("application/json");
                break;
        }
    }
}
