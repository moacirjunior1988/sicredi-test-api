package steps;

import com.aventstack.extentreports.ExtentTest;
import com.google.gson.Gson;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import geral.hooks;
import geral.utils;
import io.restassured.RestAssured;
import object.productsPost;

import java.util.List;
import java.util.Map;

public class productSteps {
    protected int statusCodeApi;
    protected ExtentTest extentTest;
    private final stepBase stepBase;
    geral.utils utils = new utils();
    private productsPost productsPost;

    public productSteps(stepBase stepBase)
    {
        this.stepBase = stepBase;
        this.extentTest = hooks.test;
        this.productsPost = new productsPost();
        stepBase.gson = new Gson();
    }

    @Given("que monto o payload de get da api de product")
    public void dadoQueMontoOPayloadDeGetDaApiDeProduct(DataTable dataTable)
    {
        extentTest.info("Dado que monto o payload de get da api de product.");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);

        switch (data.get(0).get("header"))
        {
            case "comToken":
                stepBase.request = RestAssured.given()
                        .header("Authorization", "Bearer" + stepBase.token)
                        .contentType("application/json")
                        .body(stepBase.json);
                break;

            case "semToken":
                stepBase.request = RestAssured.given()
                        .contentType("application/json");
                break;
        }
    }

    @Given("que monto o payload de post da api de product")
    public void queMontoOPayloadDePostDaApiDeProduct(DataTable dataTable) {
        extentTest.info("Dado que monto o payload de post da api de product.");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);

        switch (data.get(0).get("payload"))
        {
            case "correto":
            {
                if(data.get(0).get("title").equals("inserir")) {
                    productsPost.title = geral.utils.RandomLetras(10);
                }
                else {
                    productsPost.title = data.get(0).get("title") + " " + geral.utils.GeneratorNumber(9);
                }

                if(data.get(0).get("description").equals("inserir")){
                    productsPost.description = geral.utils.RandomLetras(40);
                }
                else{
                    productsPost.description = data.get(0).get("description");
                }

                if(data.get(0).get("price").equals("inserir")){
                    productsPost.price = geral.utils.GeneratorNumber(1500);
                }
                else {
                    productsPost.price = Integer.parseInt(data.get(0).get("price"));
                }

                if(data.get(0).get("discountPercentage").equals("inserir")){
                    productsPost.discountPercentage = Double.parseDouble(geral.utils.GeneratorNumber(15) + "." + geral.utils.GeneratorNumber(99));
                }
                else{
                    productsPost.discountPercentage = Double.parseDouble(data.get(0).get("discountPercentage"));
                }

                if(data.get(0).get("rating").equals("inserir")){
                    productsPost.rating = Double.parseDouble(geral.utils.GeneratorNumber(9) + "." + geral.utils.GeneratorNumber(99));
                }
                else{
                    productsPost.rating = Double.parseDouble(data.get(0).get("rating"));
                }

                if(data.get(0).get("stock").equals("inserir")){
                    productsPost.stock = geral.utils.GeneratorNumber(60);
                }
                else{
                    productsPost.stock =  Integer.parseInt(data.get(0).get("stock"));
                }

                if(data.get(0).get("brand").equals("inserir")){
                    productsPost.brand = geral.utils.RandomLetras(15);
                }
                else{
                    productsPost.brand = data.get(0).get("brand");
                }

                if(data.get(0).get("category").equals("inserir")){
                    productsPost.category = geral.utils.RandomLetras(15);
                }
                else{
                    productsPost.category = data.get(0).get("category");
                }

                if(data.get(0).get("thumbnail").equals("inserir")){
                    productsPost.thumbnail = "https://" + geral.utils.RandomLetras(15) + ".com";
                }
                else{
                    productsPost.thumbnail = data.get(0).get("thumbnail");
                }
            }
            break;

            case "semPayload":
                productsPost = null;
                break;
        }

        stepBase.json = stepBase.gson.toJson(productsPost);

        System.out.println("Body montado: " + stepBase.json);


        switch (data.get(0).get("header"))
        {
            case "semToken":
                stepBase.request = RestAssured.given()
                        .contentType("application/json")
                        .body(stepBase.json);
                break;
        }
    }
}
