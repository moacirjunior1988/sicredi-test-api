package geral;

import com.aventstack.extentreports.ExtentTest;
import com.google.gson.Gson;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.Random;

import static geral.hooks.test;

public class utils
{
    protected ExtentTest extentTest;
    protected static Properties props = new Properties();
    public static String pathProperties = System.getProperty("user.dir") + "/src/test/resources/configurations.properties";
    private static LocalDateTime dataAtual;
    private static Random random = new Random();
    private static Gson gson = new Gson();
    private static StringBuffer sb;

    public utils()
    {
        this.extentTest = test;
    }

    public static String getProp(String var)
    {
        try
        {
            props.load(new FileInputStream(pathProperties));
            return props.getProperty(var);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static Boolean validationPath(String path) {
        if(Files.exists(Paths.get(path)))
        {
            return true;
        }
        else
        {
            try {
                Files.createDirectories(Paths.get(path));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return true;
        }
    }

    public boolean verificaStatusCode(int statusCodeEsperado, int statusCodeApi)
    {
        boolean ok = false;
        if(statusCodeEsperado == statusCodeApi)
        {
            ok = true;
            extentTest.pass("Requisição realizada com sucesso, statusCode " + statusCodeApi + "retornado");
        }
        else
        {
            System.out.println("Requisição realizada sem sucesso, statusCode retornado foi " + statusCodeApi + " quando deveria ser " + statusCodeEsperado + " .");
            extentTest.fail("Requisição realizada sem sucesso, statusCode retornado foi " + statusCodeApi + " quando deveria ser " + statusCodeEsperado + " .");
        }
        return ok;
    }

    public boolean verificaMensagem(String msg)
    {
        boolean ok = false;
        switch (msg)
        {
            case "{password=password.required}":
                ok = true;
                extentTest.pass("Mensagem de retorno da requisição [" + msg + "] validada com sucesso!");
            break;
            
            case "{username=username.required}":
	            ok = true;
	            extentTest.pass("Mensagem de retorno da requisição [" + msg + "] validada com sucesso!");
	        break;
	        
            case "{password=password.required, username=username.required}":
            	ok = true;
	            extentTest.pass("Mensagem de retorno da requisição [" + msg + "] validada com sucesso!");
	        break;

            default:
                extentTest.fail("Mensagem de retorno da requisição [" + msg + "] inexistente!");
            break;
        }

        return ok;
    }

    public static String ReturnValueList(String[] listaValue)
    {
    	Random random = new Random();
    	
    	return listaValue[random.nextInt((listaValue.length - 1))];
    }

    //Mtodo de gerar valor dinamico
    public static int GeneratorNumber(Integer number)
    {
        return random.nextInt(number);
    }

    //Mtodo de verificao e validao dos dois ultimos digitos do cpf
    private static String calcDigVerif(String num)
    {
        Integer primDig, segDig;
        int soma = 0, peso = 10;
        for (int i = 0; i < num.length(); i++)
            soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;
        if (soma % 11 == 0 | soma % 11 == 1)
            primDig = new Integer(0);
        else
            primDig = new Integer(11 - (soma % 11));
        soma = 0;
        peso = 11;
        for (int i = 0; i < num.length(); i++)
            soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;
        soma += primDig.intValue() * 2;
        if (soma % 11 == 0 | soma % 11 == 1)
            segDig = new Integer(0);
        else
            segDig = new Integer(11 - (soma % 11));
        return primDig.toString() + segDig.toString();
    }

    //Mtodo de gerao de string dinamica passando a quantidade de letras
    public static String RandomLetras(Integer quantidadeLetras)
    {
        char[] letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        sb = new StringBuffer();
        for (int i = 1; i <= quantidadeLetras; i++)
        {
            int ch = random.nextInt(letras.length);
            sb.append (letras [ch]);
        }

        return sb.toString();
    }
}