# Automa��o de api

Est� automa��o tem a fun��o de fazer a valida��o de todas as formas poss�veis 
dentro dos cen�rios levantados na aplica��o disponibilizada.

# Ferramenta IDE
Eclipse IDE for Enterprise Java Developers (includes Incubating components)

Version: 2020-06 (4.16.0)

Build id: 20200615-1200

#Bibliotecas utilizadas na automa��o com suas vers�es

Nome Biblioteca - Vers�o

* Java JDK - 1.8.261

Nome Dependencias - Vers�o

* HttpClient - 4.5.12
* Commons-Logging - 1.2
* Commons-Codec - 1.11
* HttpCore - 4.4.13
* Json - 20180813
* Json-Path - 2.4.0
* Json-Smart - 2.3
* Accessors-Smart - 1.2
* Asm - 5.0.4
* Slf4j-Api - 1.7.25
* Httpmime - 4.5.12
* Gson - 2.8.6
* ExtentReports - 3.1.5
* FreeMarker - 2.1.23
* MongoDb-Driver - 3.3.0
* Bson - 3.3.0
* MongoDB-Driver-Core - 3.3.0
* Jsoup - 1.9.2
* Rest-Assured - 3.2.0
* Groovy - 2.4.15
* Groovy-xml - 2.4.15
* Hamcrest-Core - 1.3
* Hamcrest-Library - 1.3
* Tagsoup - 1.2.1
* Json-Path - 3.2.0
* Groovy-Json - 2.4.15
* Rest-Assurend-Common - 3.2.0
* Xml-Path - 3.2.0
* Commons-Lang3 - 3.4
* Jaxb-Api - 2.2.12
* Jaxb-Osgi - 2.2.10
* Org.Apache.Sling.Javax.Activation - 0.1.0
* Activation - 1.1.1
* Junit-Jupiter-Api - 5.6.0
* Apiguardian-Api - 1.1.0
* Opentest4j - 1.2.0
* Junit-Jupiter-Engine - 5.6.0
* Junit-Vintage-Engine - 5.6.0
* Junit - 4.1.3
* Junit-Jupiter-Params - 5.6.0
* Junit-Platform-Laucher - 1.6.0
* Junit-Jupiter-Migrationsupport - 5.6.0
* Junit-Platform-Commons - 1.6.0
* Junit-Platform-Engine - 1.6.0
* Junit-Platform-Runner - 1.6.0
* Junit-Platform-Suite-Api - 1.6.0
* Junit-Platform-Console-Standalone - 1.6.0

Dependencia adicionadas no pom do maven

* HttpClient

* HttpCore

* Rest-Assured-Common

* Json-Schema-Validator

* Json

* Json-Path

* Http-Mime

* Gson

* ExtentReports

* Rest-Assurend

* Junit-Jupiter-Api

* Junit-Vintage-Engine

* Junit

* Junit-Jupiter-params

* Junit-Platform-Launcher

* Junit-Jupiter-Migrationsupport

* Junit-Platform-Commons

* Junit-Platform-Engine

* Junit-Platform-Runner

* Junit-Platform-Console-Standalone


#Como executar os cen�rios via Junit-Runner

* 1 - Baixar todas as dependencias do maven.


* 2 - Compilar o projeto.


* 3 - Verificar se o endere�o da api configurada no "setting.properties" nas 
variavel de endere�o e porta "uriapi" e "portaapi".


* 4 - No eclipse acessar o menu "Run".


* 5 - Dentro do menu "Run" acessar a op��o "Run Configurations...".


* 6 - Abrir� uma janela com varias forma de cria��o de execu��es.


* 7 - Clicar na op��o do lado esquerdo com o nome "Junit".


* 8 - Clicar com o bot�o do lado direito do mouse em cima da op��o "Junit.


* 9 - Ir� aparecer uma janela com varias op��es, clicar na op��o "New Configuration".


* 10 - Ir� aparecer do lado direito alguns campos para serem informados para criar 
a configura��o em "Junit".


* 11 - No Campo "name" dever� informar o nome executor dos cen�rios.


* 12 - Ap�s informar o name da execu��o no campo "Test Runner" selecionar a vers�o do "Junit que est� sendo utilizada
no projeto.


* 13 - Logo acima ter� duas op��es para serem selecionada "Run a single test" e 
"Run all tests in the selected project, package or source folder".


* 14 - Na op��o "Run a single test" poder� ser executado casos de teste por 
class = cen�rio ou por method = caso de teste.


* Exemplo: "Run a single test". 
Selecionando a op��o "Run a single test", habilitar� os campos "Project, 
Test class e Test Method".

Campo "Project"

Ao selecionar a op��o "Run a single test" o campo "Project" � habilitado, com isso ter� que informar
para qual projeto deseja criar a configura��o.
No campo "Project" tem a op��es de informar o projeto escrevendo ou clicando no bot�o "Browse...".
Ap�s clicar no bot�o "Browse...", ir� aparecer um janela com todos os projetos que est�o carregados.
Selecionando um dos projetos, dever� clicar no bot�o "Ok".


Campo "Test class"


Se informar o campo "Test class", ser� executado todos os 
casos de teste que est�o dentro da class informada.
Tamb�m no campo "Test class" tem a op��o de procurar pela class que deseja executar.
Clicando no bot�o "Search" da frente do campo "Test class", dever� aparecer um janela 
com as class que tem casos de teste para execu��o e "Junit".
Ap�s selecionar a class, clicar no bot�o "Ok".


Campo "Test Method"


Para informar o campo "Test Method" e nescessario antes informar ou pesquisar uma class no campo "Test class".
Tendo informado ou consultado um class no campo "Test class", poder� no campo "Test Method" informar qual caso de teste
deseja rodar da class selecionada ou tamb�m poder� procurar clicando no bot�o "Search" da frente do campo "Test Method",
ir� aparecer uma janela com todos os casos de teste da class informada do campo "Test class".
Onde ter� os casos de teste e tamb�m a op��o "all methods", que seria executar todas aos casos de teste de uma vez, no 
lugar de um em especifico.
Ao selecionar um caso de teste especifico ou a op��o "all methods", clicar no bot�o "Ok".


* 15 - Na op��o "Run all tests in the selected project, package or source folder" poder� ser executado cen�rios por 
pacote. Onde pode ser selecionado um pacote e todos as cen�rios e casos de teste que tiverem dentro dele seram executados
automaticamente.


* Exemplo: "Run all tests in the selected project, package or source folder".
Selecionar a op��o "Run all tests in the selected project, package or source folder", habilitar� o um campo para informar 
o nome do pacote ou procurar pelo pacote que deseja ser executado.
Se selecionar em "Search" ir� aparecer um janela com o projeto, dever� ir abrindo o projeto ate chegar no package que deseja
ser executado, selecionar o package e clicar em "Ok".


* 16 - Ap�s informa a vers�o do "Junit" no campos "Test Runner", clicar no bot�o "Apply".


* 17 - Ap�s clicar no bot�o "Apply", clicar no bot�o "Run".


* 18 - Dever� iniciar a execu��o dos cen�rios e casos de teste informados na configura��o.


* 19 - Est� configura��o tamb�m e utilizada para executar dos cen�rios e casos de teste em ambiente de desenvolvimento "Debug",
� nescessario que no lugar de selecionar a op��o "Run Configurations...", seleciona a op��o "Debug Configurations...", com isso
ir� abrir a mesma janela.


* 20 - Ap�s criado pela primeira vez a configura��o independente de ser na op��o "Run" ou "Debug", poder� ser reaproveitado 
para os dois ambiente executando somente acessando o mesmo menu da configura��o mais clicando nas op��es 
"Run History" ou "Debug History" e seleciona a configura��o que voc� criou.


* 21 - Ap�s concluir a execu��o pela primeira vez ir� criar uma pasta no projeto com o nome "EvidenciaExtentReports",
dentro destas pasta estar� um arquivo html com o nome "ExtentReport".
Executando o arquivo pelo eclipse aparecer� somente c�digos html, j� se o for neste arquivo pelo windows ou linux ate a 
localiza��o fisica dele e abrir ele em um navegador, ir� apresentar um report com uma tela de dashboard e uma de 
Testes com passo a passo.


Menu - Do lado esquerdo da tela embaixo da escrita "extent" tem dois bot�es, o primeiro e a tela "Tests" e o segundo a tela
"Dashboard".


Tela Dashboard - Apresentar� um report de quantos testes foram executados e quando passos foram executados dentro destes
testes, tamb�m ter� informa��es de tempo de execu��o inicial e final, gr�fico com seus status da execu��o dividido por cores,
e descri��o de cada cor.


Tela Tests - Apresentar� o dashboard na parte de cima e os testes com seus status que foram executados do lado esquerdo,
e do lado direito estar� sendo apresentado os passo a passo de cada teste que for selecionado no lado esquerdo.
Tamb�m tem um pequena legenda de status com suas cores do lado direito entre a divisa do dashboard com o inicio da tela de 
passo a passo.


Observa��o - � poss�vel desabilitar o dashboard na tela "Tests", somente tem que clicar no icone dashboard do lado esquerdo 
acima do lado do menu. Tamb�m pode ser pesquisado um teste em espec�fico no campos search, informando o nome do teste.
No Menu Status e poss�vel filtrar os teste por status exemplo "Pass", "Fail", "Fatal" etc. S� tera que selecionar no menu status 
qual status que deseja filtrar.


* 22 - Observa��o: Pode ser criado varias configura��es e divididas conforma a escolha de quem for executar.

## Execu��o maven via linha de comando 

* 1 - Baixar o "apache-maven" e extrair do arquivo zip ou rar.


* 2 - Criar uma variavel de ambiente do sistema com o nome "MAVEN_HOME".


* 3 - Nas variavel de ambiente "MAVEN_HOME", colocar o caminho da pasta extraida do "apache-maven" Exemplo: C:\apache-maven-3.6.3.


* 4 - Na variavel de ambiente sistma com nome "PATH" acrescentar o seguinte caminho %MAVEN_HOME%\bin.


* 5 - Tamb�m verificar se o est� criado a variavel de ambiente "JAVA_HOME" com o caminho do JDK Exemplo C:\Program Files\Java\jdk1.8.0_261.


* 6 - E tamb�m tem que estar na variavel de ambiente "PATH" com o seguinte caminho %JAVA_HOME%\bin.


* 7 - Ap�s tudo estar configurado abrir a linha de comando percorer na linha de comando ate a pasta principal do projeto.


* 8 - Ai executar o seguinte comando "mvn test".
